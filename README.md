<div align='center'>
    <h1><b>🔌 Plantek Loader 🔌</b></h1>
    <img src='https://marleyplant.com/wp-content/uploads/2023/11/Logo-Dark-1.png'  />
    <p>Composer Library for creating WordPress Plugin Loaders, Enables Plugin Updates, Licensing and Dependency Checking and may already be powering one of the plugins you use.</p>

</div>

---

## 💾 **ABOUT**

Composer Library for creating WordPress Plugin Loaders, Enables Plugin Updates, restricting plugin to users that have not got a valid license and Dependency Checking.
<br />

---

## 🗒️ **USAGE**

### Basic Plugin Loader:

```php
require_once 'vendor/autoload.php';

class ExampleLoader extends PlantekPluginLoader
{
    /**
     * Return the Plugin Name for PlantekPluginLoader to Handle
     *
     * @return string
     */
    public function pluginName()
    {
        return 'ExamplePlugin';
    }

    /**
     * Return a list of dependencies for the Plugin loader
     * For Example, Advanced Custom Fields
     *
     * @return array
     */
    public function getDependencies()
    {
        return [];
    }

    /**
     * Get Licenser Settings
     *
     * @return array
     */
    public function getLicenserOptions()
    {
        return [
            "enabled" => false,
        ];
    }

    /**
     * Get Updater Settings
     *
     * @return array
     */
    public function getUpdaterOptions()
    {
        return [
            'name' => 'ExamplePlugin',
            'repository' => 'https://gitlab.com/MarleyPlant/ExamplePlugin',
            'branch' => 'master',
            'pluginFile' => __FILE__
        ];
    }

    /**
     * Initialize the plugins core
     *
     * @return void
     */
    public function init()
    {
        include_once 'plugin.php';
    }
}

add_action('plugins_loaded', function () {
    $loader = new PlantekElementorLibraryLoader();
});
```

<br/>


## 💻 **TECHNOLOGIES**

![HTML5](https://img.shields.io/badge/html5-%23E34F26.svg?style=for-the-badge&logo=html5&logoColor=white)
![PHP](https://img.shields.io/badge/php-%23777BB4.svg?style=for-the-badge&logo=php&logoColor=white)

<br />

---

## 📎 **LICENSE**

Copyright 2023 Marley Plant

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

<br />

---

## 📌 **LINKS**

[<img alt="Gitlab" src="https://img.shields.io/badge/MarleyPlant-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white" />](https://gitlab.com/MarleyPlant)

<br />

---

## 📁 **Related Projects**

- [License Manager For Woocommerce](https://github.com/wpexpertsio/license-manager-for-woocommerce)
- [otakupahp/sdk-license-manager-for-woocommerce](https://github.com/otakupahp/sdk-license-manager-for-woocommerce)
- [yahnis-elsts/plugin-update-checker"](https://marleypant.com)
- [htmlburger/carbon-fields](https://docs.carbonfields.net/)
