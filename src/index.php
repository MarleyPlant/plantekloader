<?php
require_once(__DIR__ . '/license-sdk/License.php');

use YahnisElsts\PluginUpdateChecker\v5\PucFactory;
use LMFW\SDK\License;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

class PlantekPluginLoader
{
    //Plugin Loader Variables
    var $pluginFile;
    var $pluginName;

    //Updater variables
    var $GitLabToken = 'glpat-sSKsSesh4zsipJeFM2VG';

    //Licenser Variables
    var $licenserEnabled = false;
    var $licenseServer = "https://marleyplant.com";
    var $licenseCK = '';
    var $licenseCS = '';
    var $licenseVK = '';
    var $ACFKeyFieldID = '';
    var $ACFGroupID = '';
    var $noticeDisplayed = false;

    public function __construct()
    {
        \Carbon_Fields\Carbon_Fields::boot();
        $this->setupUpdater();
        $this->setupLicenser();
        $this->licensingOptionsPage();
        $this->ACFKeyFieldID = strtolower('ptklicense_' . $this->pluginName() . '_key');
        $this->ACFGroupID = 'ptklicense_' . $this->pluginName() . '_GROUP';
        add_action('carbon_fields_theme_options_container_saved', [$this, 'redirectIFLicensesAreValid']);

        if ($this->checkDependencies() && $this->checkLicense()) {
            $this->init();
        }
    }

    public function licensingOptionsPage()
    {
        if (!$this->checkLicense()) {
            $this->ACFKeyFieldID = strtolower('ptklicense_' . $this->pluginName() . '_key');
            Container::make('theme_options', __('Plantek Licenses'))->set_page_parent("plugins.php")->add_fields(
                [
                    Field::make('text', $this->ACFKeyFieldID, $this->pluginName())
                ]
            );
        }
    }

    public function setupUpdater()
    {
        $updaterOptions = $this->getUpdaterOptions();

        $updateChecker = PucFactory::buildUpdateChecker(
            $updaterOptions['repository'],
            $updaterOptions['pluginFile'],
            $updaterOptions['name']
        );

        if(isset($updaterOptions['accessToken'])) {
            $updateChecker->setAuthentication($updaterOptions['accessToken']);
        }
        
        $updateChecker->setBranch($updaterOptions['branch']);
        $updateChecker->getVcsApi()->enableReleasePackages();
    }

    public function setupLicenser()
    {
        $licenserOptions = $this->getLicenserOptions();
        $this->licenserEnabled = $licenserOptions['enabled'];

        if (!$licenserOptions['enabled']) {
            return;
        }
        $this->pluginName = $this->pluginName();

        if (isset($licenserOptions['licenseServer']) && $licenserOptions['licenseServer'] == true) {
            $this->licenseServer = $licenserOptions['licenseServer'];
        }

        if (isset($licenserOptions['licenseServer']) && $licenserOptions['licenseServer'] == true) {
            $this->licenseServer = $licenserOptions['licenseServer'];
        }

        if (isset($licenserOptions['licenseConsumerKey']) && $licenserOptions['licenseConsumerKey'] == true) {
            $this->licenseCK = $licenserOptions['licenseConsumerKey'];
        }

        if (isset($licenserOptions['licenseConsumerSecret']) && $licenserOptions['licenseConsumerSecret'] == true) {
            $this->licenseCS = $licenserOptions['licenseConsumerSecret'];
        }

        if (isset($licenserOptions['licenseValidityKey']) && $licenserOptions['licenseValidityKey'] == true) {
            $this->licenseVK = $licenserOptions['licenseValidityKey'];
        }

        # Create an instance of the License SDK
        $this->sdk_license = new LMFW\SDK\License(
            $this->pluginName,   // The plugin name is used to manage internationalization
            $this->licenseServer, //Replace with the URL of your license server (without the trailing slash)
            $this->licenseCK, //Customer key created in the license server
            $this->licenseCS, //Customer secret created in the license server
            [], //Set an array of the products IDs on your license server (if no product validation is needed, send an empty array)
            $this->pluginName . '-License', //Set a unique value to avoid conflict with other plugins
            $this->licenseVK,  //Set a unique value to avoid conflict with other plugins
            5 //How many days the valid object will be used before calling the license server
        );
    }

    public function checkDependencies()
    {
        $dependencies = $this->getDependencies();

        foreach ($dependencies as $dependency) {
            if (!function_exists($dependency['function'])) {
                if (isset($dependency['notice'])) {
                    add_action('admin_notices', $dependency['notice']);
                }
                if (isset($dependency['required']) && $dependency['required']) {
                    return false;
                }
            } else {
                continue;
            }
        }

        return true;
    }

    public function checkLicense()
    {
        $key = get_option('_' . $this->ACFKeyFieldID);
        if (!$this->licenserEnabled) return true;
        if (!$this->sdk_license) return true;

        $status = $this->sdk_license->validate_status();

        if ($status['is_valid'] == true) {
            return true;
        } else {
            $status = $this->sdk_license->validate_status($key);
            if ($status['is_valid'] == true) {
                return true;
            } else {
                add_action('admin_notices', function () {
                    global $pagenow;
                    if (!$this->noticeDisplayed) {
?>
                        <div class="notice update-nag notice-warning inline is-dismissible">
                            <?php _e("$this->pluginName Requires a valid license to work correctly") ?>
                        </div>
<?php
                        $this->noticeDisplayed = true;
                    }
                });
                return false;
            }
        }
    }

    public function redirectIFLicensesAreValid()
    {
        $screen = get_current_screen();

        if (isset($_POST['carbon_fields_compact_input']['_ptklicense_plantekarchiver_key'])) {
            $key = $_POST['carbon_fields_compact_input']['_ptklicense_plantekarchiver_key'];
            $test = $this->checkLicense();
            $isCorrectPage = ($screen->id == "plugins_page_crb_carbon_fields_container_plantek_licenses");

            if ($isCorrectPage && $test) {
                header("Location: " . admin_url());
                die();
            }
        }
    }
}
